import 'package:bloc/bloc.dart';
import 'package:newsapp/models/news_model.dart';
import 'package:newsapp/persistance/api_repository.dart';
import 'package:equatable/equatable.dart';
part 'news_event.dart';
part 'news_state.dart';


class NewsBloc extends Bloc<NewsEvent, NewsState> {
  NewsBloc() : super(NewsInitial()) {
    final ApiRepository _apiRepository = ApiRepository();

    on<GetNewsList>((event, emit) async {
      try {
        emit(NewsLoading());
        final mList = await _apiRepository.fetchNewsList();
        emit(NewsLoaded(mList));
        if (mList.error != null) {
          emit(NewsError(mList.error));
        }
      } on NetworkError {
        emit(const NewsError("Failed to fetch data."));
      }
    });
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_storage/get_storage.dart';
import 'package:newsapp/blocs/news_bloc/news_bloc.dart';
import 'package:newsapp/models/news_model.dart';



class NewsHomeScreen extends StatefulWidget {
  const NewsHomeScreen({Key? key}) : super(key: key);

  @override
  _NewsHomeScreenState createState() => _NewsHomeScreenState();
}

class _NewsHomeScreenState extends State<NewsHomeScreen> {
  
  final NewsBloc _newsBloc = NewsBloc();


  @override
  void initState() {
    _newsBloc.add(GetNewsList());
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('News App')),
      body: _buildListNews(),
    );
  }


  Widget _buildListNews() {
    return Container(
      margin: const EdgeInsets.all(8.0),
      child: BlocProvider(
        create: (_) => _newsBloc,
        child: BlocListener<NewsBloc, NewsState>(
          listener: (context, state) {
            if (state is NewsError) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.message!),
                ),
              );
            }
          },
          child: BlocBuilder<NewsBloc, NewsState>(
            builder: (context, state) {
              if (state is NewsInitial) {
                return _buildLoading();
              } else if (state is NewsLoading) {
                return _buildLoading();
              } else if (state is NewsLoaded) {
                return _buildCard(context, state.newsModel);
              } else if (state is NewsError) {
                return _buildArticleListView();
              } else {
                return _buildNotFound();
              }
            },
          ),
        ),
      ),
    );
  }


  Widget _buildNotFound(){
    return  const Center(
      child: Text('No data found!' , style: TextStyle( fontSize: 18 , color: Colors.black38),),
    );
  }



  Future<List<Article>> getArticleList() async {

    var box = GetStorage();

    List<dynamic>? result = box.read('articleListsKey');

    List<Article> list = [];
    if(result != null) {
       list = result.map<Article>((element) {
        return Article.fromJson(element);
      }).toList();
    }

    return list;
  }


  Widget _buildLoading() => const Center(child: CircularProgressIndicator());


  Widget _buildArticleListView() {
    return FutureBuilder(
      future: getArticleList(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return _buildLoading() ;
        } else {
          var articles = snapshot.data;
          return articles!.length > 0 ? ListView.builder(
            itemCount: articles!.length,
            itemBuilder: (context, index) {

              var articleItem = articles[index];

              return _buildArticleItem(articleItem);
            },
          ) : _buildNotFound() ;
        }
      },
    );
  }


  Widget _buildCard(BuildContext context, NewsModel model) {
    return model.articles!.isNotEmpty ? ListView.builder(
      itemCount: model.articles!.length,
      itemBuilder: (context, index) {
        var articleItem = model.articles![index];
        return _buildArticleItem(articleItem);
      },
    ) : _buildNotFound();
  }

  
  Widget _buildArticleItem(Article articleItem){
    
    return Container(
      margin: const EdgeInsets.all(8.0),
      child: Card(
        child: Container(
            margin: const EdgeInsets.all(8.0),
            child:  Row(
              children: [

                Expanded(
                  flex: 6,
                  child:  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[

                      Visibility(
                        visible: articleItem.author != "",
                        child: Text("Author: ${articleItem.author}"),
                      ),

                      const SizedBox(height: 8,),
                      Text( "  ${articleItem.title}"),
                      const SizedBox(height: 8,),
                      Text("${DateTime.parse(articleItem.publishedAt.toString()).toLocal()}"),
                    ],
                  ),
                ),

                Expanded(
                  flex: 4,
                  child: Container(
                    alignment: Alignment.center,
                    height: 140,
                    child:  _buildCheckUrl('${articleItem.urlToImage}')

                  ),
                )
              ],

            )
        ),
      ),
    );
    
    
  }

  Widget _buildCheckUrl(String url) {
     return CachedNetworkImage(
       imageUrl: url ,
       imageBuilder: (context, imageProvider) => Container(
         decoration: BoxDecoration(
           image: DecorationImage(
               image: imageProvider,
               // fit: BoxFit.cover,
           ),
         ),
       ),
       placeholder: (context, url) => const CircularProgressIndicator(),
       errorWidget: (context, url, error) => const Icon(Icons.error),
     );
  }



}

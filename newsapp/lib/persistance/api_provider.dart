import 'package:dio/dio.dart';
import 'package:get_storage/get_storage.dart';
import 'package:newsapp/models/news_model.dart';

class ApiProvider {
  final Dio _dio = Dio();
  final String _url = 'https://newsapi.org/v2/top-headlines?sources=google-news&apiKey=5e540eec80dd4370add897e1d5d3091e';

  Future<NewsModel> fetchNewsList() async {
    try {
      Response response = await _dio.get(_url);

      var model = NewsModel.fromJson(response.data);

      saveArticleList(model.articles);

      return model;

    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return NewsModel.withError("Offline data load");
    }
  }
}

Future<void> saveArticleList(List<Article>? articleList) async {
  var box = GetStorage();
  List<Map<String, dynamic>> rawData = articleList!.map((e) => e.toJson()).toList();
  print('==rawData==>');
  print(rawData);
  await box.write('articleListsKey', rawData);
}
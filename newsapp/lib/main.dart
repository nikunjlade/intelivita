import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:newsapp/screen/news_home_screen.dart';

Future<void> main() async {
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'News app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const NewsHomeScreen(),
    );
  }
}


